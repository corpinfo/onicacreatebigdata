import base64
import json
import boto3
import decimal
import random
def lambda_handler(event, context):
   item = None
   dynamo_db = boto3.resource('dynamodb')
   # Update Table name if you Dynamo Table gets updated
   table = dynamo_db.Table('DemoOnicaOrders')
   decoded_record_data = [base64.b64decode(record['kinesis']['data']) for record in event['Records']]
   deserialized_data = [json.loads(decoded_record) for decoded_record in decoded_record_data]
   with table.batch_writer() as batch_writer:
       for item in deserialized_data:
           invoice = item['InvoiceNo']
           customerIdStr = item['Customer']
           if customerIdStr:
               customer = int(customerIdStr) 
           else:
               rand = random.randint(1, 50000)
               customer = rand
           orderDate = item['InvoiceDate']
           quantity = item['Quantity']
           description = item['Description']
           if description == '':
               description = "Empty Description"
               
           unitPrice = item['UnitPrice']
           country = item['Country'].rstrip()
           stockCode = item['StockCode']
           # Construct a unique sort key for this line item
           orderID = invoice + "-" + stockCode + "-"+ str(random.randint(1, 50000))
           
           item = {
               'CustomerID': decimal.Decimal(customer),
               'OrderID': orderID,
               'OrderDate': orderDate,
               'Quantity': decimal.Decimal(quantity),
               'UnitPrice': decimal.Decimal(unitPrice),
               'Description': description,
               'Country': country
            }
           batch_writer.put_item(
               Item = item
           )