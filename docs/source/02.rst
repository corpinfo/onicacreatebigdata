========================
Lab Outline
========================

Section 1 - Data Ingestion 
This section walks a user through ingesting Real Time data and storing data in Data Lake and DynamoDB.
 
#. Create certificate and required policies.
#. Create Kinesis Firehose delivery stream
#. Create Kinesis data stream
#. Create s3 bucket for Data Lake
#. Create DynamoDB table
#. Create EC2 instance using Cloud Formation Template with Kinesis Agent and Sample Data.
#. Login into EC2 instance and run python script which ingests data into S3 bucket using Kinesis Firehose Delivery Stream and Kinesis data stream.
#. Test the entire pipeline and make sure the data is landed in Data Lake.
 
Section 2 - Data Processing, ETL

#. This section walks users through cleansing and transforming the data and write data to another bucket. Then use Athena and Redshift to Process and Analyze the data.
 
Download code from Bitbucket.

#. Create a new s3 bucket for transformed data
#. Transform Data using Glue ETL Script
#. Create a Glue Crawler and run the crawler to create Database and Tables
#. Use Athena to run ad hoc queries.
#. Create Redshift instance using Cloud Formation and copy the data from transformed bucket
#. Connect to Redshift instance using a SQL Client and run some analysis queries.
 
Section 3 - Visualization

#. This lab walks users through visualizing data using Quicksight. 
#. Create Quicksight account
#. Visualize data using Quicksight
