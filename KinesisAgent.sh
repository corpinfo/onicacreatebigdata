yum install aws-kinesis-agent -y
wget https://onica-create-resources.s3.amazonaws.com/LogGenerator.zip
unzip LogGenerator.zip -d /
cd /LogGenerator
chmod a+x LogGenerator.py
mkdir /var/log/onica

cd /etc/aws-kinesis
rm agent.json
wget https://onica-create-resources.s3.amazonaws.com/agent.json

service aws-kinesis-agent start
chkconfig aws-kinesis-agent on

cd /LogGenerator
./LogGenerator.py
#tail -f /var/log/aws-kinesis-agent/aws-kinesis-agent.log