create external schema orders_ext from data catalog
database 'demo-onica-orders-dw'
iam_role 'arn:aws:iam::<AWS Account Id>:role/RedshiftCopyUnload'
region 'us-east-1';


CREATE SCHEMA IF NOT EXISTS onica;
 
drop table if exists onica.orders;
 
CREATE TABLE IF NOT EXISTS onica.orders
(
    invoiceno bigint ENCODE zstd
    ,stockcode VARCHAR(100) ENCODE raw
    ,description VARCHAR(500) ENCODE zstd
    ,quantity bigint  ENCODE zstd
    ,invoicedate VARCHAR(50)  ENCODE zstd
    ,unitprice double precision ENCODE zstd
    ,country varchar(50) encode zstd
    ,customerid bigint null encode zstd
    ,total double precision ENCODE zstd
)
DISTSTYLE KEY
 DISTKEY (invoiceno)
SORTKEY (
    stockcode
);
 
COPY onica.orders
FROM 's3://onica-orders-transformed-bucket-<USER NAME>/'
credentials 'aws_iam_role=arn:aws:iam::<AWS Account Id>:role/RedshiftCopyUnload'
FORMAT AS PARQUET;
 
select * from onica.orders limit 100;