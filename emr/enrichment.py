"""
A pyspark script to carry out a simple enrichment process,
by joining source with 3rd-party vendors data sets.
"""
from pyspark import SparkConf, SparkContext
from pyspark.sql import SQLContext
from pyspark.sql import SparkSession

# initialize spark session
conf = SparkConf()
sc = SparkContext(conf=conf)
spark = SparkSession(sc)

# define config params
source_bucket = "onica-orders-transformed-bucket-<USER NAME>"
artifacts_bucket = "onica-emr-bucket-artifacts-<USER NAME>"
enrichment_bucket = "onica-emr-bucket-enrichment-<USER NAME>"

third_party_zone = "3rd-party"
enrichment_zone = "enrichment"
third_party_table_name = "tax_rate.csv"

# ingest source data into spark cluster
source_df = spark.read.format("parquet").load("s3://" + source_bucket)

# ingest 3rd party data into spark cluster
tax_rate_df = spark.read.load("s3://" + enrichment_bucket + "/" + third_party_zone + "/" + third_party_table_name, 
              format="com.databricks.spark.csv", header="true", inferSchema="true")

# apply transformation
result_df = source_df.join(tax_rate_df,
            source_df.country == tax_rate_df.country,
            how = "left_outer").drop(tax_rate_df.tax_year).drop(tax_rate_df.country)
result_df = result_df.withColumn('total', result_df['quantity'] * result_df['unitprice'] * result_df['tax'])

# persist data into s3
result_df.write.parquet("s3://" + enrichment_bucket + "/" + enrichment_zone)