from pyspark.context import SparkContext
from pyspark.sql.functions import *
from awsglue.context import GlueContext
from awsglue.job import Job
import sys
from awsglue.utils import getResolvedOptions
  
def load_data_frame(schema, table, glue_context, table_mapping={}, table_casting={}, required_fields=[]):
    # get type2 dynamic frame
    dynamic_frame = glue_context.create_dynamic_frame.from_catalog(database=schema, table_name=table)
  
    # rename fields
    for key, value in table_mapping.items():
        dynamic_frame = dynamic_frame.rename_field(key, value)
  
    # cast fields
    for key, cast in table_casting.items():
        dynamic_frame = dynamic_frame.resolveChoice(specs=[(key, cast)])
  
    # create data frame
    output_data_frame = dynamic_frame.toDF()
  
    # apply filters
    for requirement in required_fields:
        output_data_frame = output_data_frame.where(requirement + " is NOT NULL")
  
    return output_data_frame
      
  
glueContext = GlueContext(SparkContext.getOrCreate())
spark = glueContext.spark_session
glueJob = Job(glueContext)
args = getResolvedOptions(sys.argv, ['JOB_NAME'])
glueJob.init(args['JOB_NAME'], args)
  
source_database = "demo-onica-orders-dw"
source_table = "onica_orders_bucket_<USER NAME>"
  
column_name_mapping = {
    "col0" : "invoiceno",
    "col1" : "stockcode",
    "col2" : "description",
    "col3" : "quantity",
    "col4" : "invoicedate",
    "col5" : "unitprice",
    "col6" : "customerid",
    "col7" : "country",
    "partition_0" : "year",
    "partition_1" : "month",
    "partition_2" : "day",
    "partition_3" : "hour"
    }
column_type_casting = {
    "invoiceno" : "cast:bigint",
    "stockcode" : "cast:string",
    "description" : "cast:string",
    "quantity" : "cast:bigint",
    "invoicedate" : "cast:string",
    "unitprice" : "cast:double",
    "customerid" : "cast:bigint",
    "country" : "cast:string",
    "year" : "cast:string",
    "month" : "cast:string",
    "day" : "cast:string",
    "hour" : "cast:string"
    }
  
columns_required = ["invoiceno"]
  
source_df = load_data_frame(source_database, source_table, glueContext, column_name_mapping, column_type_casting, columns_required)
 
tax_rate_file_path = "s3://onica-create-static-<USER NAME>/tax_rate.csv"
tax_rate_df = spark.read.option("header", "true").option("delimiter", ",").csv(tax_rate_file_path)
 
result_df = source_df.join(tax_rate_df,
            source_df.country == tax_rate_df.country,
            how = "left_outer").drop(tax_rate_df.tax_year).drop(tax_rate_df.country)
 
result_df = result_df.withColumn('total', result_df['quantity'] * result_df['unitprice'] * result_df['tax'])
 
  
result_df.createOrReplaceTempView("onica_orders_view")
result_df = result_df.alias('result_df')
  
onic_orders_df = spark.sql("SELECT * FROM onica_orders_view")
output_dir = "s3://onica-orders-transformed-bucket-<USER NAME>/"
onic_orders_df.write.parquet(output_dir, mode="append", partitionBy=['year','month','day','hour'], compression="gzip")
  
glueJob.commit()