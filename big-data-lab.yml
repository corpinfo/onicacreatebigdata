AWSTemplateFormatVersion: "2010-09-09"
Description: Big Data Demo Stack
Parameters:
  # pAMI:
  #   Type: String
  #   Description: "Image ID"
  #   Default: "ami-035b3c7efe6d061d5"

  pKeyPair:
    Type: AWS::EC2::KeyPair::KeyName
    Description: "MUST Create Key Pair before deploying template, Key Pair Name"
    Default: "bigdata-demo-kp"

  pVpc:
    Type: AWS::EC2::VPC::Id
    Description: "US EAST 1 - VPC ID - https://console.aws.amazon.com/vpc/home?region=us-east-1#subnets:search=us-east-1a;sort=SubnetId"
    Default: "vpc-a7b00fdd"

  pSubnet:
    Type: AWS::EC2::Subnet::Id
    Description: "UPDATE --Subnet ID - us-east-1a - https://console.aws.amazon.com/vpc/home?region=us-east-1#subnets:search=us-east-1a;sort=SubnetId"
    Default: "subnet-c7b2818d"

  pS3BucketName:
    Type: String
    Description: "Unique S3 Bucket Name"
    Default: "onica-orders-bucket-"


  pS3DestinationBucketName:
    Type: String
    Description: "Unique S3 Bucket Name"
    Default: "onica-orders-transformed-bucket-"

Resources:
  # EC2 INSTANCE
  DemoEc2:
    Type: AWS::EC2::Instance
    Properties: 
      IamInstanceProfile: !Ref Ec2InstanceProfile
      ImageId: ami-035b3c7efe6d061d5
      AvailabilityZone: us-east-1a
      InstanceType: t2.micro
      KeyName: !Ref pKeyPair
      SecurityGroupIds: 
        - !Ref DemoSecurityGroup
      SubnetId: !Ref pSubnet
      Tags:
        - Key: Name
          Value: BigDataDemoEc2
      UserData:
        Fn::Base64: !Sub |
          #!/bin/bash -xe
          wget https://onica-create-resources.s3.amazonaws.com/KinesisAgent.sh
          chmod +x KinesisAgent.sh
          sudo ./KinesisAgent.sh
  
  # IAM ROLES  - TODO SCALE BACK PERMISSIONS
  Ec2IamRole:
    Type: AWS::IAM::Role
    Properties: 
      AssumeRolePolicyDocument:
        Version: "2012-10-17"
        Statement:
          -
            Effect: "Allow"
            Principal:
              Service:
                - "ec2.amazonaws.com" 
            Action: 
              - "sts:AssumeRole"
      Description: BigDataDemoRole
      ManagedPolicyArns: 
        - arn:aws:iam::aws:policy/AdministratorAccess
      RoleName: BigDataDemoRole

  KinesisDeliveryRole:
    Type: AWS::IAM::Role
    Properties: 
      AssumeRolePolicyDocument:
        Version: "2012-10-17"
        Statement:
          -
            Effect: "Allow"
            Principal:
              Service:
                - "firehose.amazonaws.com" 
            Action: 
              - "sts:AssumeRole"
      Description: BigDataDemoRole
      ManagedPolicyArns: 
        - arn:aws:iam::aws:policy/AdministratorAccess
      RoleName: demo-firehose-delivery-role

  LambdaRole:
    Type: AWS::IAM::Role
    Properties: 
      AssumeRolePolicyDocument:
        Version: "2012-10-17"
        Statement:
          -
            Effect: "Allow"
            Principal:
              Service:
                - "lambda.amazonaws.com" 
            Action: 
              - "sts:AssumeRole"
      Description: BigDataDemoRole
      ManagedPolicyArns: 
        - arn:aws:iam::aws:policy/AdministratorAccess
      RoleName: LambdaOrderProcessRole

  GlueRole:
    Type: AWS::IAM::Role
    Properties: 
      AssumeRolePolicyDocument:
        Version: "2012-10-17"
        Statement:
          -
            Effect: "Allow"
            Principal:
              Service:
                - "glue.amazonaws.com" 
            Action: 
              - "sts:AssumeRole"
      Description: BigDataDemoRole
      ManagedPolicyArns: 
        - arn:aws:iam::aws:policy/AdministratorAccess
      RoleName: GlueCrawlerRole

  # IAM PROFILES
  Ec2InstanceProfile:
    Type: AWS::IAM::InstanceProfile
    Properties: 
      InstanceProfileName: BigDataDemoRoleProfile
      Roles: 
        - !Ref Ec2IamRole

  # SECURITY GROUPS
  DemoSecurityGroup:
    Type: AWS::EC2::SecurityGroup
    Properties: 
      GroupDescription: DemoSG
      GroupName: DemoSG
      VpcId: !Ref pVpc
      SecurityGroupEgress: 
        - CidrIp: 0.0.0.0/0
          FromPort: -1
          IpProtocol: -1
          ToPort: -1
      SecurityGroupIngress: 
        - CidrIp: 0.0.0.0/0
          FromPort: 22
          IpProtocol: tcp
          ToPort: 22
        - CidrIp: 0.0.0.0/0
          FromPort: 5439
          IpProtocol: tcp
          ToPort: 5439

  SGIngressRule:
    Type: "AWS::EC2::SecurityGroupIngress"
    # DependsOn: DemoSecurityGroup
    Properties:
      GroupId: !Ref DemoSecurityGroup
      IpProtocol: -1
      FromPort: -1
      ToPort: -1
      SourceSecurityGroupId: !Ref DemoSecurityGroup

  # KINESIS STREAMS
  KinesisDataStream:
    Type: AWS::Kinesis::Stream
    Properties: 
      Name: DemoOnicaOrders
      RetentionPeriodHours: 24
      ShardCount: 1
      StreamEncryption: 
        EncryptionType: KMS
        KeyId: !Ref DemoKmsKey

  KinesisDeliveryStream:
    Type: AWS::KinesisFirehose::DeliveryStream
    Properties: 
      DeliveryStreamName: DemoPurchaseLogs
      DeliveryStreamType: DirectPut
      S3DestinationConfiguration: 
        BucketARN: !GetAtt s3Bucket.Arn
        BufferingHints: 
          IntervalInSeconds: 60
          SizeInMBs: 5
        CompressionFormat: UNCOMPRESSED
        RoleARN: !GetAtt KinesisDeliveryRole.Arn
  
  # KMS KEY
  DemoKmsKey:
    Type: AWS::KMS::Key
    Properties: 
      Description: Demo KMS key for Kinesis
      KeyPolicy: 
        Version: "2012-10-17"
        Statement:
          -
            Sid: "Enable IAM User Permissions"
            Effect: "Allow"
            Principal: 
              AWS: 
                - !Join [ '', [ 'arn:aws:iam::', !Ref "AWS::AccountId", ':root' ] ] 
            Action: 
              - "kms:*"
            Resource: "*"

  #S3 BUCKET FOR KINESIS
  s3Bucket:
    Type: AWS::S3::Bucket
    Properties:
      BucketName: !Ref pS3BucketName

  s3DestinationBucket:
    Type: AWS::S3::Bucket
    Properties:
      BucketName: !Ref pS3DestinationBucketName

  # DYNAMO TABLE
  OrdersTable:
    Type:  AWS::DynamoDB::Table
    Properties:
      TableName: DemoOnicaOrders
      AttributeDefinitions:
        - AttributeName: CustomerID
          AttributeType: N
        - AttributeName: OrderID
          AttributeType: S
      KeySchema: 
        - AttributeName: CustomerID
          KeyType: HASH
        - AttributeName: OrderID
          KeyType: RANGE
      ProvisionedThroughput:
        ReadCapacityUnits: 5
        WriteCapacityUnits: 5

  # LAMBDA  - TODO MAKE SURE S3 BUCKET IS PUBLIC
  ProcessOrdersLambda:
    Type: AWS::Lambda::Function
    Properties: 
      Code: 
        S3Bucket: onica-create-resources
        S3Key: lambda_function.py.zip
      FunctionName: DemoProcessOrders
      Handler: lambda_function.lambda_handler
      Role: !GetAtt LambdaRole.Arn
      Runtime: python2.7
      Timeout: 60

  # LAMBDA EVENT MAPPING
  KinesisEventMapping:
    Type: AWS::Lambda::EventSourceMapping
    Properties: 
      BatchSize: 100
      EventSourceArn: !GetAtt KinesisDataStream.Arn
      FunctionName: DemoProcessOrders
      StartingPosition: LATEST

  # REDSHIFT CLUSTER
  RedshiftCluster:
    Type: AWS::Redshift::Cluster
    Properties: 
      AvailabilityZone: us-east-1a
      ClusterIdentifier: demo-onica-orders-dw
      ClusterType: multi-node
      DBName: orders_db
      Encrypted: false
      IamRoles: 
        - !Join ['', ['arn:aws:iam::', !Ref "AWS::AccountId", ':role/aws-service-role/redshift.amazonaws.com/AWSServiceRoleForRedshift']]
        - !Join ['', ['arn:aws:iam::', !Ref "AWS::AccountId", ':role/aws-service-role/redshift.amazonaws.com/RedshiftCopyUnload']]
      MasterUsername: awsuser
      MasterUserPassword: Onica1234!
      NodeType: dc2.large
      NumberOfNodes: 2
      Port: 5439
      PubliclyAccessible: true
      VpcSecurityGroupIds: 
        - !Ref DemoSecurityGroup

  # GLUE RESOURCES
  GlueDatabase:
    Type: AWS::Glue::Database
    Properties: 
      CatalogId: !Ref "AWS::AccountId"
      DatabaseInput: 
        Description: BigData Demo
        Name: demoonicaorders

  # GlueTable:
  #   Type: AWS::Glue::Table
  #   Properties: 
  #     CatalogId: !Ref "AWS::AccountId"
  #     DatabaseName: !Ref GlueDatabase
  #     TableInput: 
  #       Description: BigData Demo Table
  #       Name: onica_order_logs
  #       PartitionKeys: 
  #         - Name: year
  #         - Name: month
  #         - Name: day
  #         - Name: hour

  GlueConnection:
    Type: AWS::Glue::Connection
    Properties: 
      CatalogId: !Ref "AWS::AccountId"
      ConnectionInput:  # example jdbc:redshift://onica-orders-dw.coocqdtjeocj.us-east-1.redshift.amazonaws.com:5439/orders_db
        ConnectionProperties: {
          "JDBC_CONNECTION_URL": !Join ['', ['jdbc:redshift://', !GetAtt "RedshiftCluster.Endpoint.Address", ':5439/orders_db']],
          "USERNAME": "awsuser",
          "PASSWORD": "Onica1234!"
        }
        ConnectionType: JDBC
        Description: BigDataDemo
        Name: onica-orders

  GlueCrawler:
    Type: AWS::Glue::Crawler
    Properties: 
      # Configuration: String
      # CrawlerSecurityConfiguration: String
      DatabaseName: demo-onica-orders-dw
      Description: BigData Demo
      Name: OrdersCrawler
      Role: !Ref GlueRole
      Targets: 
        S3Targets: 
          - Path: !Join ['', ['s3://', !Ref "s3Bucket"]]